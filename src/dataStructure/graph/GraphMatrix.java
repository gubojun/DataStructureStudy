package dataStructure.graph;

import java.util.ArrayList;
import java.util.List;

public class GraphMatrix<E> {
    GraphType type = GraphType.UNDIRECTED_GRAPH;//图的类型

    enum GraphType {UNDIRECTED_GRAPH, DIRECTED_GRAPH}

    //顶点表
    List<Vertex<E>> vertexList = new ArrayList<>();
    //边表
    List<Edge<E>> edgeList = new ArrayList<>();
    int[][] edges;

    public GraphMatrix(ArrayList<E> vertexList) {
        createVertexList(vertexList);
    }

    public GraphMatrix(E[] vertexList) {
        createVertexList(vertexList);
    }

    public GraphMatrix(String vertexList) {
        createVertexList(vertexList);
    }

    public int getVertexNum() {
        return vertexList.size();
    }

    public void addEdge(E startValue, E endValue, int weight) {
        Vertex<E> startV = searchVertexByValue(startValue);
        Vertex<E> endV = searchVertexByValue(endValue);
        if (startV != null && endV != null) {
            Edge<E> edge = new Edge<>(startV, endV, weight);
            edgeList.add(edge);
            edges[vertexList.indexOf(startV)][vertexList.indexOf(endV)] = edge.weight;
            if (type == GraphType.UNDIRECTED_GRAPH) {
                edgeList.add(new Edge<>(endV, startV));
                edges[vertexList.indexOf(endV)][vertexList.indexOf(startV)] = edge.weight;
            }
        } else {
            System.out.println("没有找到顶点值");
        }
    }

    public void addEdge(E startValue, E endValue) {
        addEdge(startValue, endValue, 1);
    }

    public void information() {
        System.out.println("顶点信息:");
        for (Vertex<E> vertex : vertexList) {
            System.out.print(vertex.value + " ");
        }
        System.out.println("\n边表:");
        for (Edge<E> edge : edgeList) {
            if (type == GraphType.UNDIRECTED_GRAPH) {
                System.out.print(edge.startV.value + "<->" + edge.endV.value + " ");
            } else {
                System.out.println("{" + edge.startV.value + "->" + edge.endV.value + " 权重:" + edge.weight + "} ");
            }
        }

        System.out.println("\n边的权重表:");
        showEdges();
    }

    public void showEdges() {
        for (int i = 0; i < getVertexNum(); i++) {
            for (int j = 0; j < getVertexNum(); j++) {
                System.out.print(edges[i][j] + " ");
            }
            System.out.println();
        }
    }

    public int getVertexIndex(E value) {
        Vertex<E> vertex = searchVertexByValue(value);
        if (vertex != null) {
            return vertexList.indexOf(vertex);
        }
        return -1;
    }

    private Vertex<E> searchVertexByValue(E value) {
        for (Vertex<E> vertex : vertexList) {
            if (vertex.value.equals(value)) {
                return vertex;
            }
        }
        return null;
    }

    private void createVertexList(ArrayList<E> vertexList) {
        for (E value : vertexList) {
            this.vertexList.add(new Vertex<>(value));
        }
        edges = new int[getVertexNum()][getVertexNum()];
    }

    private void createVertexList(E[] vertexList) {
        for (E value : vertexList) {
            this.vertexList.add(new Vertex<>(value));
        }
        edges = new int[getVertexNum()][getVertexNum()];
    }

    private void createVertexList(String vertexList) {
        for (char value : vertexList.toCharArray()) {
            this.vertexList.add((Vertex<E>) new Vertex<>(String.valueOf(value)));
        }
        edges = new int[getVertexNum()][getVertexNum()];
    }
}
