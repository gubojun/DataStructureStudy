package dataStructure.graph;

import java.util.ArrayList;
import java.util.List;

/**
 * 图的顶点
 */
public class Vertex<E> {
    //到该顶点的边列表
    List<Edge> edgeList = new ArrayList<>();
    E value;

    public Vertex(E value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value.toString();
    }
}
