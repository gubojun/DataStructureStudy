package dataStructure.graph;

/**
 * 图的边
 */
public class Edge<E> {
    Vertex<E> startV;//起点
    Vertex<E> endV;//终点

    public Edge(Vertex<E> startV, Vertex<E> endV) {
        this(startV, endV, 1);
    }

    public Edge(Vertex<E> startV, Vertex<E> endV, int weight) {
        this.startV = startV;
        this.endV = endV;
        this.weight = weight;
    }

    /**
     * 权重
     */
    int weight;
}
