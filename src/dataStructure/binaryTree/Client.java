package dataStructure.binaryTree;

public class Client {
    public static void main(String[] args) {
        BinaryTree<String> bt = new BinaryTree<>();
//        A
//       / \
//      B   C
//     / \   \
//    D   E   F
//     \
//      G
        bt.insert(null, "A", true);
        bt.insert("A", "B", true);
        bt.insert("A", "C", false);
        bt.insert("B", "D", true);
        bt.insert("B", "E", false);
        bt.insert("C", "F", false);
        bt.insert("D", "G", false);
//        bt.insert("E", "H", true);
//        bt.insert("E", "I", false);
        bt.insert("F", "J", false);

        bt.preOrder();
        bt.inOrder();
        bt.postOrder();
        bt.printTree();
//        System.out.println("B right:"+bt.rightChildCount("B"));
//        A
//         \
//          B
//           \
//            C
//             \
//              D
//             ...
        BinSearchTree<String> bst = new BinSearchTree<>();
        bst.insert("A");
        bst.insert("B");
        bst.insert("C");
        bst.insert("D");
        bst.insert("E");
        bst.insert("F");
        bst.insert("G");
        System.out.println("\n二叉排序树：");
        bst.preOrder();
        bst.inOrder();
        bst.postOrder();
        bst.printTree();
    }
}
