package dataStructure.binaryTree;

public class TreeNode<E> {
    private static final int LH = 1;    //左高
    private static final int EH = 0;    //等高
    private static final int RH = -1;   //右高
    public E element;
    /**
     * 权重
     **/
    public int weight;
    /**
     * 左子树
     */
    public TreeNode<E> left;
    /**
     * 右子树
     */
    public TreeNode<E> right;
    /**
     * 父节点
     */
    public TreeNode<E> parent;
    int balance = EH;   //平衡因子，只能为1或0或-1

    public TreeNode(E e) {
        element = e;
    }

    /**
     * 是否是父节点的左孩子
     *
     * @return
     */
    public boolean isParentLeft() {
        return parent != null && parent.left == this;
    }
}