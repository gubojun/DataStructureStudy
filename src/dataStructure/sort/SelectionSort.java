package dataStructure.sort;

/**
 * 选择排序 O(n^2)
 */
public class SelectionSort extends DataCrol {
    public void sort(int[] array) {
        for (int i = 0; i < array.length - 1; i++) {
            int min = i;
            for (int j = min + 1; j < array.length; j++) {
                //找出最小值
                if (array[j] < array[min]) {
                    min = j;
                }
            }
            //交换元素
            if (i != min) {
                int temp = array[i];
                array[i] = array[min];
                array[min] = temp;
            }
        }
    }

    public static void main(String[] args) {
        SelectionSort selectionSort = new SelectionSort();
        int[] array = DataCrol.createRandomArray(20);
        DataCrol.print(array);
        selectionSort.sort(array);
        DataCrol.print(array);
        selectionSort.timeTest();
    }
}
