package dataStructure.sort;
// 分类 ------------- 内部比较排序
// 数据结构 ---------- 数组
// 最差时间复杂度 ---- 最坏情况为输入序列是降序排列的,此时时间复杂度O(n^2)
// 最优时间复杂度 ---- 最好情况为输入序列是升序排列的,此时时间复杂度O(n)
// 平均时间复杂度 ---- O(n^2)
// 所需辅助空间 ------ O(1)
// 稳定性 ------------ 稳定

/**
 * 插入排序
 */
public class InsertionSort extends DataCrol {
    @Override
    public void sort(int[] array) {
        for (int i = 1; i < array.length; i++) {
            int get = array[i];
            int j = i - 1;
            for (; j >= 0 && array[j] > get; j--) {
                array[j + 1] = array[j];
            }
            array[j + 1] = get;
        }
    }

    public static void main(String[] args) {
        InsertionSort insertionSort = new InsertionSort();
        int[] array = DataCrol.createRandomArray(20);
        DataCrol.print(array);
        insertionSort.sort(array);
        DataCrol.print(array);
        insertionSort.timeTest();
    }
}
