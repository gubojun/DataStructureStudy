package dataStructure.sort;


// 分类 -------------- 内部比较排序
// 数据结构 ---------- 数组
// 最差时间复杂度 ---- O(n^2)
// 最优时间复杂度 ---- O(nlogn)
// 平均时间复杂度 ---- O(n^2)
// 所需辅助空间 ------ O(1)
// 稳定性 ------------ 稳定

/**
 * 二分插入排序
 */
public class InsertionSortDichotomy extends DataCrol {
    @Override
    public void sort(int[] array) {
        for (int i = 1; i < array.length; i++) {
            int get = array[i];
            int left = 0;
            int right = i - 1;
            //找到插入位置
            while (left <= right) {
                int mid = (left + right) / 2;
                if (array[mid] > get)
                    right = mid - 1;
                else
                    left = mid + 1;
            }
            for (int j = i - 1; j >= left; j--) {
                array[j + 1] = array[j];
            }
            array[left] = get;
        }
    }

    public static void main(String[] args) {
        InsertionSortDichotomy insertionSort = new InsertionSortDichotomy();
        int[] array = DataCrol.createRandomArray(20);
        DataCrol.print(array);
        insertionSort.sort(array);
        DataCrol.print(array);
        insertionSort.timeTest();
    }
}
