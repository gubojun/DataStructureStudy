package dataStructure.sort;

import java.util.Random;

public abstract class DataCrol {
    private static Random random = new Random(47);

    public static int[] createRandomArray(int size) {
        int[] result = new int[size];
        for (int i = 0; i < size; i++) {
            result[i] = random.nextInt(100);
        }
        return result;
    }

    public static void print(int[] array) {
        for (int anArray : array) {
            System.out.print(anArray + " ");
        }
        System.out.println();
    }

    protected void swap(int A[], int i, int j) {
        int temp = A[i];
        A[i] = A[j];
        A[j] = temp;
    }

    public abstract void sort(int[] array);

    protected void timeTest() {
        int ARRAY_SIZE = 50000;
        int[] array = DataCrol.createRandomArray(ARRAY_SIZE);
        long start = System.currentTimeMillis();
        sort(array);
        System.out.println("\ntime:" + (System.currentTimeMillis() - start));
    }

    protected void timeTest(int arraySize) {
        int[] array = DataCrol.createRandomArray(arraySize);
        long start = System.currentTimeMillis();
        sort(array);
        System.out.println("\ntime:" + (System.currentTimeMillis() - start));
    }
}
