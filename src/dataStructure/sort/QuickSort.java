package dataStructure.sort;
// 分类 ------------ 内部比较排序
// 数据结构 --------- 数组
// 最差时间复杂度 ---- 每次选取的基准都是最大（或最小）的元素，导致每次只划分出了一个分区，需要进行n-1次划分才能结束递归，时间复杂度为O(n^2)
// 最优时间复杂度 ---- 每次选取的基准都是中位数，这样每次都均匀的划分出两个分区，只需要logn次划分就能结束递归，时间复杂度为O(nlogn)
// 平均时间复杂度 ---- O(nlogn)
// 所需辅助空间 ------ 主要是递归造成的栈空间的使用(用来保存left和right等局部变量)，取决于递归树的深度，一般为O(logn)，最差为O(n)
// 稳定性 ---------- 不稳定

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

/**
 * 快速排序
 */
public class QuickSort extends DataCrol {
    /**
     * 优化
     *
     * @param array
     * @param left
     * @param right
     * @return
     */
    int partition3(int[] array, int left, int right) {
        //三数取中
        int mid = (left + right) / 2;
        if (array[mid] > array[right]) {
            swap(array, mid, right);
        }
        if (array[left] > array[right]) {
            swap(array, left, right);
        }
        if (array[mid] > array[left]) {
            swap(array, mid, left);
        }
        return partition2(array, left, right);
    }

    // 58 55 93 61 61 29 68 00 22 07
    // 07 55 93 61 61 29 68 00 22 07
    // 07 55 93 61 61 29 68 00 22 93
    // 07 55 22 61 61 29 68 00 22 93
    // 07 55 22 61 61 29 68 00 61 93
    // 07 55 22 00 61 29 68 00 61 93
    // 07 55 22 00 61 29 68 61 61 93
    // 07 55 22 00 29 58 68 61 61 93
    //                OK
    int partition2(int[] array, int left, int right) {
        //1.先取最左边的数作为基准
        //2.执行函数后划分为基准数左边的数都比基准数小，右边的数都比基准数大
        //3.返回基准数下标
        //固定的切分方式
        int key = array[left];
        while (left < right) {
            while (array[right] >= key && right > left) {//从后半部分向前扫描
                right--;
            }
            array[left] = array[right];
            while (array[left] <= key && right > left) {//从前半部分向后扫描
                left++;
            }
            array[right] = array[left];
        }
        array[right] = key;
        return right;
    }

    // 58 55 93 61 61 29 68 00 22 07
    // P  i
    // 58 55 29 61 61 93 68 00 22 07
    // P     i
    // 58 55 29 00 61 93 68 61 22 07
    // P        i
    // 58 55 29 00 22 93 68 61 61 07
    // P           i
    // 58 55 29 00 22 07 68 61 61 93
    // P              i
    // 07 55 29 00 22 58 68 61 61 93
    // P              OK
    int partition(int A[], int left, int right) { // 划分函数
//        int pivot = A[right];               // 这里每次都选择最后一个元素作为基准
//        int tail = left - 1;                // tail为小于基准的子数组最后一个元素的索引
//        for (int i = left; i < right; i++) { // 遍历基准以外的其他元素
//            if (A[i] < pivot) {             // 把小于等于基准的元素放到前一个子数组末尾
//                swap(A, ++tail, i);
//            }
//        }
//        swap(A, tail + 1, right);           // 最后把基准放到前一个子数组的后边，剩下的子数组既是大于基准的子数组
//        // 该操作很有可能把后面元素的稳定性打乱，所以快速排序是不稳定的排序算法
//        return tail + 1;                    // 返回基准的索引

        int pivot = A[left];
        int tail = left;
        for (int i = left + 1; i <= right; i++) {
            if (A[i] < pivot) {
                swap(A, ++tail, i);
            }
        }
        swap(A, left, tail);
        return tail;
    }

    void quickSort(int A[], int left, int right) {
        if (left >= right)
            return;
        int pivot_index = partition3(A, left, right); // 基准的索引
        quickSort(A, left, pivot_index - 1);
        quickSort(A, pivot_index + 1, right);
    }

    /**
     * 快速排序非递归（栈）
     *
     * @param array
     */
    void quickSortIteration(int array[]) {
        LinkedList<Integer> stack = new LinkedList<>();
        int left, right, pivotPos;
        stack.push(0);
        stack.push(array.length - 1);
        while (stack.size() != 0) {
            right = stack.pop();
            left = stack.pop();
            pivotPos = partition(array, left, right);
            if (left < pivotPos - 1) {
                stack.push(left);
                stack.push(pivotPos - 1);
            }
            if (right > pivotPos + 1) {
                stack.push(pivotPos + 1);
                stack.push(right);
            }
        }
    }

    @Override
    public void sort(int[] array) {
//        quickSort(array, 0, array.length - 1);
        quickSortIteration(array);
    }

    public static void main(String[] args) {
        QuickSort quickSort = new QuickSort();
        int[] array = DataCrol.createRandomArray(10);
//        int[] array = new int[]{6, 3, 7, 4, 1};
        DataCrol.print(array);
        quickSort.sort(array);
        DataCrol.print(array);
        quickSort.timeTest(700000);
    }
}
