package dataStructure.sort;

// 分类 -------------- 内部比较排序
// 数据结构 ---------- 数组
// 最差时间复杂度 ---- 根据步长序列的不同而不同。已知最好的为O(n(logn)^2)
// 最优时间复杂度 ---- O(n)
// 平均时间复杂度 ---- 根据步长序列的不同而不同。
// 所需辅助空间 ------ O(1)
// 稳定性 ------------ 不稳定

/**
 * 希尔排序
 */
public class ShellSort extends DataCrol {
    @Override
    public void sort(int[] array) {
        int h = 0;
        int size = array.length;
        while (h <= size) {// 生成初始增量
            h = 3 * h + 1;
        }
//        h = size / 2;
        while (h > 0) {
            for (int i = h; i < size; i++) {
                int j = i - h;
                int get = array[i];
                while (j >= 0 && array[j] > get) {
                    array[j + h] = array[j];
                    j = j - h;
                }
                array[j + h] = get;
            }
            h = (h - 1) / 3;// 递减增量
//            h /= 2;
        }
    }

    public static void main(String[] args) {
        ShellSort insertionSort = new ShellSort();
        int[] array = DataCrol.createRandomArray(20);
        DataCrol.print(array);
        insertionSort.sort(array);
        DataCrol.print(array);
        insertionSort.timeTest(10000000);
    }
}
